# final-project-adonis-main-bersama



## Getting started

Asslamualaikum wr, wb.

Hello, welcome to my final project of jcc nodejs adonis backend
this project is used to fullfil the final task of the bootcamp.

link heroku : https://main-bersama-genta.herokuapp.com/

authored by 
Genta Adiyasa - @gentadiasa

Thank you for your attention. 
Best Regards.

## endpoint

the endpoint to use are :

POST
/register 
/login
/venues
/venues/:id
/venues/:venue_id/fields
/fields/:field_id/bookings

GET
/venues
/venues/:venue_id/fields
/venues/:venue_id/fields/:id
/fields/:field_id/bookings
/fields/:id

PUT
/venues/:venue_id/fields/:id
/fields/:field_id/bookings/:id
/bookings/:id