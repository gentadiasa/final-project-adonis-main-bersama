"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class RegisterVenueValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            name: Validator_1.schema.string({}, [
                Validator_1.rules.alpha({ allow: ['space', 'underscore', 'dash'] }),
                Validator_1.rules.minLength(5)
            ]),
            address: Validator_1.schema.string({}, [
                Validator_1.rules.minLength(10)
            ]),
            phone: Validator_1.schema.string({}, [
                Validator_1.rules.mobile({
                    locales: ['id-ID'],
                })
            ])
        });
        this.messages = {
            'required': 'the {{field}} is required to register new venue',
            'name.alpha': 'the {{field}} must be characters without number and symbols',
            'phone.mobile': 'the {{field}} must be properly formatted as a phone format.',
        };
    }
}
exports.default = RegisterVenueValidator;
//# sourceMappingURL=RegisterVenueValidator.js.map