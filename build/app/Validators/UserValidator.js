"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class UserValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            name: Validator_1.schema.string.optional(),
            email: Validator_1.schema.string({}, [
                Validator_1.rules.email()
            ]),
            password: Validator_1.schema.string({}, [
                Validator_1.rules.minLength(6),
                Validator_1.rules.confirmed()
            ])
        });
        this.messages = {};
    }
}
exports.default = UserValidator;
//# sourceMappingURL=UserValidator.js.map