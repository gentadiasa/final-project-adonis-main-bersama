"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class BookingValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            title: Validator_1.schema.string(),
            play_date_start: Validator_1.schema.date({}, [
                Validator_1.rules.after('today')
            ]),
            play_date_end: Validator_1.schema.date({}, [
                Validator_1.rules.after('today')
            ]),
        });
        this.messages = {
            'required': 'the {{field}} is required to register new venue',
            'nama.alpha': 'the {{field}} must be characters without number and symbols',
            'booked_date.before': 'the {{field}} must be booked 1 days before.',
        };
    }
}
exports.default = BookingValidator;
//# sourceMappingURL=BookingValidator.js.map