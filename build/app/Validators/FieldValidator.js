"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class FieldValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            name: Validator_1.schema.string({}, [
                Validator_1.rules.alpha({ allow: ['space', 'underscore', 'dash'] }),
                Validator_1.rules.minLength(5)
            ]),
            type: Validator_1.schema.enum(['futsal', 'mini soccer', 'basketball'])
        });
        this.messages = {
            'required': 'the {{field}} is required to register new venue',
            'name.alpha': 'the {{field}} must be characters without number and symbols',
            'enum.choices': 'value dari {{ field }} tidak boleh selain {{ options.choices }}'
        };
    }
}
exports.default = FieldValidator;
//# sourceMappingURL=FieldValidator.js.map