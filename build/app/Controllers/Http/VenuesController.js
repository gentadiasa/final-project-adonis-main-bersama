"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const RegisterVenueValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/RegisterVenueValidator"));
const Venue_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Venue"));
class VenuesController {
    async index({ response }) {
        try {
            let venue = await Venue_1.default.all();
            response.created({ status: 'success', message: 'sukses get all data venue!', venue });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async store({ request, response }) {
        try {
            await request.validate(RegisterVenueValidator_1.default);
            let newVenue = new Venue_1.default();
            newVenue.name = request.input('name');
            newVenue.address = request.input('address');
            newVenue.phone = request.input('phone');
            await newVenue.save();
            response.created({ status: 'success', message: 'new venue created!', newId: newVenue.id });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async show({ response, params }) {
        try {
            let venue = await Venue_1.default.query().where('id', params.id).preload('field').firstOrFail();
            response.created({ status: 'success', message: 'sukses get data venue by id!', venue });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async update({ request, response, params }) {
        try {
            await request.validate(RegisterVenueValidator_1.default);
            let venue = await Venue_1.default.findOrFail(params.id);
            venue.name = request.input('name');
            venue.address = request.input('address');
            venue.phone = request.input('phone');
            await venue.save();
            response.created({ status: 'success', message: 'sukses memperbarui data venue!' });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async delete({ response, params }) {
        try {
            let venue = await Venue_1.default.findOrFail(params.id);
            venue.delete();
            response.created({ status: 'success', message: 'sukses delete venue tersebut!' });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
}
exports.default = VenuesController;
//# sourceMappingURL=VenuesController.js.map