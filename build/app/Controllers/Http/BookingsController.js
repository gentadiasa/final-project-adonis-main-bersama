"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Booking_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Booking"));
const Field_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Field"));
const BookingValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/BookingValidator"));
class BookingsController {
    async index({}) {
    }
    async store({ request, response, params, auth }) {
        const field = await Field_1.default.findByOrFail('id', params.field_id);
        const user = auth.user;
        const payload = await request.validate(BookingValidator_1.default);
        const booking = new Booking_1.default();
        booking.playDateStart = payload.play_date_start;
        booking.playDateEnd = payload.play_date_end;
        booking.title = payload.title;
        booking.related('field').associate(field);
        user.related('myBooking').save(booking);
        return response.created({ status: 'success', data: booking });
    }
    async show({ response, params }) {
        const booking = await Booking_1.default.query().where('id', params.id).preload('players', (userQuery) => {
            userQuery.select(['name', 'email', 'id']);
        }).withCount('players').firstOrFail();
        return response.ok({ status: 'sucess', data: booking });
    }
    async update({}) {
    }
    async destroy({}) {
    }
    async join({ response, params, auth }) {
        const booking = await Booking_1.default.findOrFail(params.id);
        let user = auth.user;
        await booking.related('players').sync([user.id]);
        return response.ok({ status: 'success', data: 'Sucessfully join' });
    }
}
exports.default = BookingsController;
//# sourceMappingURL=BookingsController.js.map