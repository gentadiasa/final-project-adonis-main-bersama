"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
const User_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/User"));
const UserValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/UserValidator"));
class AuthController {
    async register({ request, response }) {
        try {
            let payload = await request.validate(UserValidator_1.default);
            let newUser = await User_1.default.create({
                name: payload.name,
                email: payload.email,
                password: payload.password
            });
            response.created({ status: 'success', data: newUser });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async login({ auth, request, response, }) {
        try {
            const loginSchema = Validator_1.schema.create({
                email: Validator_1.schema.string({ trim: true }),
                password: Validator_1.schema.string({ trim: true }),
            });
            const payload = await request.validate({ schema: loginSchema });
            const token = await auth.use('api').attempt(payload.email, payload.password);
            return response.ok({ status: 'success', data: token });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
}
exports.default = AuthController;
//# sourceMappingURL=AuthController.js.map