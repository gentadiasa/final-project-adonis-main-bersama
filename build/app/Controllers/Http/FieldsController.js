"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Field_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Field"));
const Venue_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Venue"));
const FieldValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/FieldValidator"));
class FieldsController {
    async index({ response, params }) {
        try {
            const venue = await Venue_1.default.findByOrFail('id', params.venue_id);
            let field_by_venue = await Field_1.default.query().where('venue_id', params.venue_id);
            response.created({ status: 'success', message: 'sukses get data fields!', data: { venue_name: venue.name, field_by_venue } });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async store({ request, response, params }) {
        await request.validate(FieldValidator_1.default);
        const venue = await Venue_1.default.findByOrFail('id', params.venue_id);
        let newField = new Field_1.default();
        newField.name = request.input('name');
        newField.type = request.input('type');
        await newField.related('venue').associate(venue);
        return response.created({ status: 'success', message: 'field created!', data: newField });
    }
    async show({ response, params }) {
        try {
            let field = await Field_1.default.query().where('id', params.id).preload('bookings', (bookingQuery) => {
                bookingQuery.select(['title', 'play_date_start', 'play_date_end']);
            }).firstOrFail();
            response.created({ status: 'success', message: 'berhasil get data field', data: field });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async update({ request, response, params }) {
        try {
            await request.validate(FieldValidator_1.default);
            let field = await Field_1.default.findOrFail(params.id);
            field.name = request.input('name');
            field.type = request.input('type');
            await field.save();
            response.created({ status: 'success', message: 'sukses memperbarui data field!' });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
    async delete({ request, response, params }) {
        try {
            await request.validate(FieldValidator_1.default);
            let field = await Field_1.default.findOrFail(params.id);
            field.delete();
            response.created({ status: 'success', message: 'sukses delete fields!' });
        }
        catch (e) {
            response.badRequest({ status: 'failed', message: e.message });
        }
    }
}
exports.default = FieldsController;
//# sourceMappingURL=FieldsController.js.map