"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Route_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Core/Route"));
Route_1.default.get('/', async () => {
    return { hello: 'world' };
});
Route_1.default.group(() => {
    Route_1.default.resource('/venues', 'VenuesController').apiOnly().middleware({
        '*': ['auth']
    });
    Route_1.default.resource('venues.fields', 'FieldsController').apiOnly().middleware({
        '*': ['auth']
    });
    Route_1.default.resource('fields.bookings', 'BookingsController').apiOnly().middleware({
        '*': ['auth']
    });
});
Route_1.default.post('/login', 'AuthController.login').as('auth.login');
Route_1.default.post('/register', 'AuthController.register').as('auth.register');
Route_1.default.get('/fields/:id', 'FieldsController.show').middleware(['auth']).as('fields.show');
Route_1.default.get('/bookings/:id', 'BookingsController.show').middleware(['auth']).as('bookings.show');
Route_1.default.put('/bookings/:id', 'BookingsController.join').middleware(['auth']).as('bookings.join');
//# sourceMappingURL=routes.js.map