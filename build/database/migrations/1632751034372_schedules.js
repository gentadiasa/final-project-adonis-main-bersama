"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Schema_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Lucid/Schema"));
class Schedules extends Schema_1.default {
    constructor() {
        super(...arguments);
        this.tableName = 'schedules';
    }
    async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('user_id').unsigned().references('id').inTable('users');
            table.integer('booking_id').unsigned().references('id').inTable('bookings');
        });
    }
    async down() {
        this.schema.dropTable(this.tableName);
    }
}
exports.default = Schedules;
//# sourceMappingURL=1632751034372_schedules.js.map