import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Fields extends BaseSchema {
  protected tableName = 'fields'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name')
      table.enu('type', ['futsal', 'mini soccer', 'basketball' ])
      table.integer('venue_id').unsigned().references('id').inTable('venues')

      /**
       * Uses timestampz for PostgreSQL and DATETIME2 for MSSQL
       */
      //  table.timestamp('created_at', { useTz: true }).notNullable()
      //  table.timestamp('updated_at', { useTz: true }).notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
