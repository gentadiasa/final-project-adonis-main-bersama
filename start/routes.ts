/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { 
    messages: 'Welcome to Main Bersama Api by Genta - jcc nodejs 2021 batch1!',
    list_api: 'here is the route list of api : register, login, venues, fields, booking'  
 }
})

// Route.get('/venues/', 'VenuesController.index').as('venues.index')
// Route.post('/venues', 'VenuesController.store').as('venues.store')
// Route.get('/venues/:id', 'VenuesController.show').as('venues.show')
// Route.put('/venues/:id', 'VenuesController.update').as('venues.update')
// Route.delete('/venues/:id', 'VenuesController.delete').as('venues.delete')

Route.group(() => {
  Route.resource('/venues', 'VenuesController').apiOnly().middleware({
    '*': ['auth']
  })
  Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
    '*': ['auth']
  })
  Route.resource('fields.bookings', 'BookingsController').apiOnly().middleware({
    '*': ['auth']
  })
})

Route.post('/login', 'AuthController.login').as('auth.login')
Route.post('/register', 'AuthController.register').as('auth.register')

Route.get('/fields/:id', 'FieldsController.show').middleware(['auth']).as('fields.show')

Route.get('/bookings/:id', 'BookingsController.show').middleware(['auth']).as('bookings.show')
Route.put('/bookings/:id', 'BookingsController.join').middleware(['auth']).as('bookings.join')