import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import RegisterVenueValidator from 'App/Validators/RegisterVenueValidator'
// import BookingValidator from 'App/Validators/BookingValidator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'
// import Venues from 'Database/migrations/1631915477134_venues'

export default class VenuesController {
    public async index({response}: HttpContextContract){
        try{
            // let venue = await Database.from('venues')
            let venue = await Venue.all();
            response.created({status: 'success', message: 'sukses get all data venue!', venue})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

    public async store({request, response}: HttpContextContract){
        try{
            await request.validate(RegisterVenueValidator);
            // let newVenue = await Database.table('venues').returning('id').insert({
            //     name: request.input('name'),
            //     address: request.input('address'),
            //     phone: request.input('phone')
            // })
            let newVenue = new Venue()
            newVenue.name = request.input('name')
            newVenue.address = request.input('address')
            newVenue.phone = request.input('phone')

            await newVenue.save()
            response.created({status: 'success', message: 'new venue created!', newId: newVenue.id})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

    public async show({response, params}: HttpContextContract){
        try{
            // let venue = await Database.from('venues').where('id', params.id)
            // let venue = await Venue.find(params.id)
            let venue = await Venue.query().where('id', params.id).preload('field').firstOrFail()
            response.created({status: 'success', message: 'sukses get data venue by id!', venue})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
        // let name = request.qs().name
        // response.status(200).json({name, b: params.b})
    }
    
    public async update({request, response, params}: HttpContextContract){
        try{
            await request.validate(RegisterVenueValidator);
            // await Database.from('venues').where('id', params.id).update({
            //     name: request.input('name'),
            //     address: request.input('address'),
            //     phone: request.input('phone')
            // })
            let venue = await Venue.findOrFail(params.id)
            venue.name = request.input('name')
            venue.address = request.input('address')
            venue.phone = request.input('phone')
            await venue.save()
            response.created({status: 'success', message: 'sukses memperbarui data venue!'})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

    public async delete({response, params}: HttpContextContract){
        try{
            // await Database.from('venues').where('id', params.id).del()
            let venue = await Venue.findOrFail(params.id)
            venue.delete();
            response.created({status: 'success', message: 'sukses delete venue tersebut!'})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

}
