import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field';
import Venue from 'App/Models/Venue';
import FieldValidator from 'App/Validators/FieldValidator'

export default class FieldsController {

    public async index({response, params}: HttpContextContract){
        try{
            const venue = await Venue.findByOrFail('id', params.venue_id)
            let field_by_venue = await Field.query().where('venue_id', params.venue_id)
            response.created({status: 'success', message: 'sukses get data fields!', data: {venue_name: venue.name, field_by_venue}})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

    public async store({request, response, params}: HttpContextContract){
        await request.validate(FieldValidator);
        const venue = await Venue.findByOrFail('id', params.venue_id)
        // let newField = await Database.table('fields').returning('id').insert({
        //     name: request.input('name'),
        //     type: request.input('type'),
        // })
        // console.log('vv', venue)
        let newField = new Field()
        newField.name = request.input('name')
        newField.type = request.input('type')
        // newField.venue_id = request.input('type')
        // await newField.save()
        await newField.related('venue').associate(venue)

        return response.created({status: 'success', message: 'field created!', data: newField})
    }

    public async show({response, params}: HttpContextContract){
        try{
            // let field = await Database.from('fields').where('id', params.id)
            // let field = await Field.find(params.id)
            let field = await Field.query().where('id', params.id).preload('bookings', (bookingQuery) => {
                bookingQuery.select(['title', 'play_date_start', 'play_date_end'])
            }).firstOrFail()
            response.created({status: 'success', message: 'berhasil get data field', data: field})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }
   

    public async update({request, response, params}: HttpContextContract){
        try{
            await request.validate(FieldValidator);
            // await Database.from('fields').where('id', params.id).update({
            //     name: request.input('name'),
            //     address: request.input('address'),
            //     type: request.input('type')
            // })
            let field = await Field.findOrFail(params.id)
            field.name = request.input('name')
            field.type = request.input('type')
            await field.save()
            response.created({status: 'success', message: 'sukses memperbarui data field!'})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

    public async delete({request, response, params}: HttpContextContract){
        try{
            await request.validate(FieldValidator);
            // await Database.from('fields').where('id', params.id).del()
            let field = await Field.findOrFail(params.id)
            field.delete();
            response.created({status: 'success', message: 'sukses delete fields!'})
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }
}
