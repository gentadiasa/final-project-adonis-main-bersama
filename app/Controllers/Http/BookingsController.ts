import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import BookingValidator from 'App/Validators/BookingValidator'

export default class BookingsController {
  public async index ({}: HttpContextContract) {
  }

  public async store ({request, response, params, auth}: HttpContextContract) {
    const field = await Field.findByOrFail('id', params.field_id)
    const user = auth.user!
    const payload = await request.validate(BookingValidator)

    const booking = new Booking()
    booking.playDateStart = payload.play_date_start
    booking.playDateEnd = payload.play_date_end
    booking.title = payload.title

    booking.related('field').associate(field)
    // booking.related('user_booked').associate(user)
    user.related('myBooking').save(booking)

    return response.created({status: 'success', data: booking})
  }

  public async show ({response, params}: HttpContextContract) {
    const booking = await Booking.query().where('id', params.id).preload('players', (userQuery)=>{
      userQuery.select(['name', 'email', 'id'])
    }).withCount('players').firstOrFail()
    return response.ok({ status: 'sucess', data: booking })
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({}: HttpContextContract) {
  }

  public async join ({ response, params, auth}: HttpContextContract) {
    const booking = await Booking.findOrFail(params.id)
    let user = auth.user!
    await booking.related('players').sync([user.id])
    return response.ok({status: 'success', data: 'Sucessfully join'})
  }
}
