import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator';
// import Mail from '@ioc:Adonis/Addons/Mail'
export default class AuthController {
    public async register({ request, response }: HttpContextContract){
        try{
            let payload = await request.validate(UserValidator);
            let newUser = await User.create({
                name: payload.name,
                email: payload.email,
                password: payload.password
            })

            // property send does not exist in mail

            // await Mail.send((message) => {
            //     message
            //       .from('info@example.com')
            //       .to('virk@adonisjs.com')
            //       .subject('Welcome Onboard!')
            //       .htmlView('emails/welcome', { name: 'Virk' })
            //   })
            response.created({ status: 'success', data: newUser })
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }

    public async login({ auth, request, response, }: HttpContextContract){
        try{
            const loginSchema = schema.create({
                email: schema.string({trim: true}),
                password: schema.string({trim: true}),
            })
            const payload = await request.validate({ schema: loginSchema })
            const token = await auth.use('api').attempt(payload.email, payload.password)
            return response.ok({ status: 'success', data: token })
        }catch(e){
            response.badRequest({ status: 'failed', message: e.message })
        }
    }
}
